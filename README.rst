About
=====

*openmat-plugins* is a collection of plug-ins for *openmatDB*'s python plug-in server.

*openmat-plugins* is released under Apache License Version 2.0.

For details see NOTICE and LICENSE files.

The plug-ins depend on following packages:

* python3
* pandas
* xlrd
* matplotlib
* mpld3
* NumPy
* pyopenmatdb

Please see the documentation for more details on *openmatDB* and its components:
https://openmatDB.readthedocs.io/en/latest/
# This file is part of the openmat-plugins project.
#
# Copyright 2020 Thies Hecker
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pandas import read_excel


def run(session, filename=None, sheetname=None, collection=None):
    df = read_excel(filename, sheetname)
    record_list = df.to_dict('records')

    valid_records = session.run_plugin('add_records', *[collection, record_list])

    return valid_records


def register():
    return {
        'name': 'Import records from Excel',
        'UI_elements': [
            {'type': 'TextInput',
             'label': 'Excel file name',
             'default_value': '../files/test_data.xlsx',
             'data_type': 'str'},
            {'type': 'TextInput',
             'label': 'Sheet name',
             'default_value': 'Employees',
             'data_type': 'str'},
            {'type': 'TextInput',
             'label': 'Collection',
             'default_value': 'Employees',
             'data_type': 'str'},
        ],
        'help': 'Imports records from an excel sheet. First row of excel sheet will be used as attribute names.\n'
                'Returns the DB entry ids of successfully imported records.',
        'return_type': 'text',
    }


# if __name__ == '__main__':
#     session = SessionSync(api_url='http://0.0.0.0:5000/v1')
#     collection = 'Employees'
#     excel_sheet = '../test_data.xlsx'
#     sheetname = 'Employees'
#     collection = 'Employees'
#     result = run(session, excel_sheet, sheetname, collection)
#     print(result)
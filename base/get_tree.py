# This file is part of the openmat-plugins project.
#
# Copyright 2020 Thies Hecker
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

def run(session):
    collections = session.run_plugin('get_collections')

    tree = {}
    for collection in collections:
        records = session.run_plugin('get_records', *[collection, {}])
        record_ids = [record['_id'] for record in records]
        tree[collection] = record_ids

    return tree


def register():
    return {
        'name': 'Get DB tree',
        'UI_elements': [],
        'help': 'Returns all collections and their corresponding record IDs.',
        'return_type': 'text'
    }


# if __name__ == '__main__':
#     session = SessionSync(api_url='http://0.0.0.0:5000/v1')
#     result = run(session)
#     print(result)

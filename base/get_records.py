# This file is part of the openmat-plugins project.
#
# Copyright 2020 Thies Hecker
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


def run(session, collection=None, query_dict=None):
    records = session.db_client.get_records(collection=collection, query_dict=query_dict)
    return records


def register():
    return {
        'name': 'Get records',
        'UI_elements': [
            {'type': 'TextInput',
             'label': 'Collection',
             'default_value': ''},
            {'type': 'TextInput',
             'label': 'Query dict',
             'default_value': ''},
        ],
        'return_type': 'text'
    }

#
# if __name__ == '__main__':
#     session = SessionSync(api_url='http://0.0.0.0:5000/v1')
#     collection = 'Employees'
#     query_dict = {'age': {'$gte': 40}}
#     result = run(session, collection, query_dict)
#     print(result)

# This file is part of the openmat-plugins project.
#
# Copyright 2020 Thies Hecker
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import matplotlib.pyplot as plt
import mpld3


def run(session, collection, query_dict, xlabel_attr, yvalue_attr, ui_type):

    records = session.run_plugin('get_records', *[collection, query_dict])

    xlabels = [record[xlabel_attr] for record in records]
    yvalues = [record[yvalue_attr] for record in records]

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.bar(xlabels, yvalues)
    ax.set_xlabel(xlabel_attr)
    ax.set_ylabel(yvalue_attr)
    ax.legend()

    # handle both output for desktop (standard matplotlib) and web UI (html)
    if ui_type == 'desktop':
        return ax
    if ui_type == 'browser':
        #
        ax.set_xticks(range(len(xlabels)))
        ax.set_xticklabels(xlabels)
        # ax_html = mpld3.fig_to_dict(fig)
        ax_html = mpld3.fig_to_html(fig)
        return ax_html


def register():
    return {
        'name': 'Create bar chart',
        'UI_elements': [
            {'type': 'TextInput',
             'label': 'Collection',
             'default_value': ''},
            {'type': 'TextInput',
             'label': 'Query dict',
             'default_value': ''},
            {'type': 'TextInput',
             'label': 'Attribute for x-axis',
             'default_value': ''},
            {'type': 'TextInput',
             'label': 'Attribute for y-axis',
             'default_value': ''},
            {'type': 'TextInput',
             'label': 'UI type',
             'default_value': ''}
        ],
        'return_type': 'pyplot',
        'help': 'Creates a bar chart for the records in the collection. The query dict can be used to filter records.'
                'Attributes for y-axis have to be of numeric types. The \'UI type\' attribute must be set to the '
                'correct UI - either \'desktop\' or \'browser\'.'
    }


# if __name__ == '__main__':
#
#     session = SessionSync(api_url='http://0.0.0.0:5000/v1')
#     collection = "Employees"
#     query_dict = {'age': {'$gte': 40}}
#     xlabel_attr = 'name'
#     yvalue_attr = 'age'
#     result = run(session, collection, query_dict, xlabel_attr, yvalue_attr)
#     plt.show()

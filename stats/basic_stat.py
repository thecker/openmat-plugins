# This file is part of the openmat-plugins project.
#
# Copyright 2020 Thies Hecker
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np


def run(session, collection=None, query_dict=None, attribute=None):
    records = session.run_plugin('get_records', *[collection, query_dict])

    results = {}

    attribute_values = [record[attribute] for record in records]
    attribute_values = np.array(attribute_values)

    results['samples'] = len(attribute_values)
    results['min'] = attribute_values.min()
    results['max'] = attribute_values.max()
    results['mean'] = attribute_values.mean()
    results['std'] = attribute_values.std()

    return results


def register():
    return {
        'name': 'Basic statistics',
        'UI_elements': [
            {'type': 'TextInput',
             'label': 'Collection',
             'default_value': 'Employees'},
            {'type': 'TextInput',
             'label': 'Query dict',
             'default_value': ''},
            {'type': 'TextInput',
             'label': 'Attribute',
             'default_value': ''},
        ],
        'help': 'Calculates min, max, mean and standard dev. for a given numeric property for a query. \nIf no query '
                'dict. is provided, the calculation will be performed for the whole collection'
    }


# if __name__ == '__main__':
#     session = SessionSync(api_url='http://0.0.0.0:5000/v1')
#     collection = 'Employees'
#     query_dict = {'age': {'$gte': 40}}
#     results = run(session, collection, query_dict, 'age')
#     print(results)
